﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.AzureConnector {

	public interface IAzureApiAuthenticator {

		Task<string> GetAccessTokenAsync(string authority, string resource, string scope);
	}

	public class AzureApiAuthenticator : IAzureApiAuthenticator {

#if DEBUG
		public int TokenRequestCount => authenticator.TokenRequestCount;
#endif

		private readonly BaseAzureApiAuthenticator authenticator;

		public AzureApiAuthenticator(AzureKeyVaultOptions options) {
			this.authenticator = new BaseAzureApiAuthenticator(() => options);
		}

		public Task<string> GetAccessTokenAsync(string authority, string resource, string scope)
			=> authenticator.GetAccessTokenAsync(authority, resource, scope);
	}

	internal class BaseAzureApiAuthenticator : IAzureApiAuthenticator {

		private class CachedToken {
			public string Token { get; set; }
			public DateTimeOffset ExpiresOn { get; set; }
		}

		private static readonly Encoding TextEncoding = Encoding.UTF8;
		private static readonly TimeSpan TokenValidityLimit = TimeSpan.FromMinutes(5);

		private readonly Func<AzureKeyVaultOptions> optionsAccessor;
		private readonly ConcurrentDictionary<string, CachedToken> cachedTokens = new ConcurrentDictionary<string, CachedToken>();

#pragma warning disable IDE1006 // Naming Styles

		private AzureKeyVaultOptions _options;
		protected AzureKeyVaultOptions Options {
			get {
				if (_options == null) {
					_options = optionsAccessor.Invoke();
				}
				return _options;
			}
		}

#pragma warning restore IDE1006 // Naming Styles

		public BaseAzureApiAuthenticator(Func<AzureKeyVaultOptions> optionsAccessor) {
			this.optionsAccessor = optionsAccessor;
		}

		public async Task<string> GetAccessTokenAsync(string authority, string resource, string scope) {

			//Logger.LogTrace("Azure API access token requested");

			var cachedTokenId = string.Format(
				"{0}:{1}:{2}",
				 Convert.ToBase64String(TextEncoding.GetBytes(authority ?? "")),
				 Convert.ToBase64String(TextEncoding.GetBytes(resource ?? "")),
				 Convert.ToBase64String(TextEncoding.GetBytes(scope ?? ""))
			);

			if (cachedTokens.TryGetValue(cachedTokenId, out CachedToken cachedToken) && DateTimeOffset.Now < cachedToken.ExpiresOn - TokenValidityLimit) {
				//Logger.LogTrace("Returning cached access token with remaining validity of {AccessTokenRemainingMinutes} minutes", (cachedToken.ExpiresOn - DateTimeOffset.Now).TotalMinutes.ToString("F1"));
				return cachedToken.Token;
			}

			//Logger.LogTrace("Acquiring access token");

			var result = await requestAccessTokenAsync(authority, resource, scope);

			if (result == null) {
				throw new InvalidOperationException("Failed to obtain the JWT token");
			}

			//Logger.LogTrace("Access token acquired");

			cachedToken = new CachedToken() {
				Token = result.AccessToken,
				ExpiresOn = result.ExpiresOn,
			};
			cachedTokens.AddOrUpdate(cachedTokenId, cachedToken, (k, v) => cachedToken);

			return result.AccessToken;
		}

#if DEBUG
		public int TokenRequestCount { get; private set; }
#endif

		private async Task<AuthenticationResult> requestAccessTokenAsync(string authority, string resource, string scope) {
#if DEBUG
			++TokenRequestCount;
#endif
			var authContext = new AuthenticationContext(authority);
			var clientCred = new ClientCredential(Options.AppId, Options.AppSecret);
			return await authContext.AcquireTokenAsync(resource, clientCred);
		}
	}
}
