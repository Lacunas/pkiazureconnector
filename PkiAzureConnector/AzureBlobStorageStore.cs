﻿using Lacuna.Pki.Stores;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#if !NETSTANDARD2_0
using Microsoft.Azure;
#endif

namespace Lacuna.Pki.AzureConnector {

	public class AzureBlobStorageStore : ISimpleStore {

		/*
			https://msdn.microsoft.com/en-us/library/azure/dd135715.aspx

			A container name must be a valid DNS name, conforming to the following naming rules:
			- Container names must start with a letter or number, and can contain only letters, numbers, and the dash (-) character.
			- Every dash (-) character must be immediately preceded and followed by a letter or number; consecutive dashes are not permitted in container names.
			- All letters in a container name must be lowercase.
			- Container names must be from 3 through 63 characters long.
		 */
		public const string DefaultContainerName = "lacuna-pki-store";

#if !NETSTANDARD2_0
		public static AzureBlobStorageStore CreateFromSettingName(string settingName, string containerName = DefaultContainerName) {
			var connectionString = CloudConfigurationManager.GetSetting(settingName);
			return CreateFromConnectionString(connectionString, containerName);
		}
#endif

		public static AzureBlobStorageStore CreateFromConnectionString(string connectionString, string containerName = DefaultContainerName) {
			var storageAccount = CloudStorageAccount.Parse(connectionString);
			return CreateFromStorageAccount(storageAccount, containerName);
		}

		public static AzureBlobStorageStore CreateFromStorageAccount(CloudStorageAccount storageAccount, string containerName = DefaultContainerName) {
			var blobClient = storageAccount.CreateCloudBlobClient();
			return CreateFromBlobClient(blobClient, containerName);
		}

		public static AzureBlobStorageStore CreateFromBlobClient(CloudBlobClient blobClient, string containerName = DefaultContainerName) {
			var container = blobClient.GetContainerReference(containerName);
			return CreateFromContainerReference(container);
		}

		public static AzureBlobStorageStore CreateFromContainerReference(CloudBlobContainer container) {
			return new AzureBlobStorageStore(container);
		}

#pragma warning disable IDE1006 // Naming Styles
		private readonly CloudBlobContainer _container;
		private bool _containerInitialized;
#pragma warning restore IDE1006 // Naming Styles

		private AzureBlobStorageStore(CloudBlobContainer container) {
			this._container = container;
		}

		public byte[] Get(byte[] index) => Task.Run(() => GetAsync(index)).GetAwaiter().GetResult();

		public async Task<byte[]> GetAsync(byte[] index) {
			var container = await GetContainerAsync();
			var blob = container.GetBlockBlobReference(getBlobName(index));
			if (!await blob.ExistsAsync()) {
				return null;
			}
			byte[] content;
			using (var buffer = new MemoryStream()) {
				await blob.DownloadToStreamAsync(buffer);
				content = buffer.ToArray();
			}
			return content;
		}

		public void Put(byte[] index, byte[] content) => Task.Run(() => PutAsync(index, content)).GetAwaiter().GetResult();

		public async Task PutAsync(byte[] index, byte[] content) {
			var container = await GetContainerAsync();
			var blob = container.GetBlockBlobReference(getBlobName(index));
			//blob.UploadFromByteArray(content, 0, content.Length);
			using (var buffer = new MemoryStream(content)) {
				await blob.UploadFromStreamAsync(buffer);
			}
		}

		protected async Task<CloudBlobContainer> GetContainerAsync() {
			if (!_containerInitialized) {
				await _container.CreateIfNotExistsAsync();
				_containerInitialized = true;
			}
			return _container;
		}

		private static string getBlobName(byte[] index) {
			/*
			 * https://msdn.microsoft.com/en-us/library/azure/dd135715.aspx
			 * 
			 * A blob name must conforming to the following naming rules:
			 * - A blob name can contain any combination of characters.
			 * - A blob name must be at least one character long and cannot be more than 1,024 characters long.
			 * - Blob names are case-sensitive.
			 * - Reserved URL characters must be properly escaped.
			 * - The number of path segments comprising the blob name cannot exceed 254. A path segment is the string between consecutive delimiter characters (e.g., the forward slash '/') that corresponds to the name of a virtual directory.
			 */
			//return Convert.ToBase64String(index).Replace("+", "%2B").Replace("=", "%3D");
			return String.Join("", index.Select(b => b.ToString("X2")));
		}
	}
}
