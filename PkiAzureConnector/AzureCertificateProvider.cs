﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.AzureConnector {

	public interface IAzureCertificateProvider {

		Task<PKCertificateWithKey> GetCertificateWithKeyAsync(string certificateName);
	}

	public class AzureCertificateProvider : IAzureCertificateProvider {

		private readonly AzureKeyVaultService azureKeyVaultService;

		public AzureCertificateProvider(AzureKeyVaultOptions options, IAzureApiAuthenticator azureApiAuthenticator = null) {
			azureKeyVaultService = new AzureKeyVaultService(() => options, azureApiAuthenticator ?? new BaseAzureApiAuthenticator(() => options));
		}

		public Task<PKCertificateWithKey> GetCertificateWithKeyAsync(string certificateName)
			=> azureKeyVaultService.GetCertificateWithKeyAsync(certificateName);
	}
}
