﻿using Microsoft.Azure.KeyVault.Models;
using Microsoft.Azure.KeyVault.WebKey;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.AzureConnector {

	public class AzureKey : IPrivateKey {

		private readonly AzureKeyVaultService azureKeyVaultService;
		private readonly KeyBundle key;

		private PublicKey publicKey;
		public PublicKey PublicKey => publicKey ??= azureKeyVaultService.GetPublicKey(key);

		internal AzureKey(AzureKeyVaultService azureKeyVaultService, KeyBundle key) {
			this.azureKeyVaultService = azureKeyVaultService;
			this.key = key;
		}

		public ISignatureCsp GetSignatureCsp(DigestAlgorithm digestAlgorithm)
			=> new AzureKeyCsp(azureKeyVaultService, key, digestAlgorithm);

		public PKCertificateWithKey GetCertificateWithKey(PKCertificate certificate) {
			if (!certificate.SubjectPublicKey.IsEquivalent(PublicKey)) {
				throw new ArgumentException($"The certificate's public key does not match the azure key");
			}
			return new PKCertificateWithKey(certificate, this);
		}

		public IEncryptionCsp GetEncryptionCsp()
			=> new AzureKeyCsp(azureKeyVaultService, key, null);
	}

	internal class AzureKeyCsp : ISignatureCsp, IEncryptionCsp {

		private readonly AzureKeyVaultService azureKeyVaultService;
		private readonly KeyBundle key;
		private readonly DigestAlgorithm digestAlgorithm;

		public AzureKeyCsp(AzureKeyVaultService azureKeyVaultService, KeyBundle key, DigestAlgorithm digestAlgorithm) {
			this.azureKeyVaultService = azureKeyVaultService;
			this.key = key;
			this.digestAlgorithm = digestAlgorithm;
		}

		public bool CanSign => true;

		public bool CanDecrypt => true;

		public byte[] SignData(byte[] buffer) => SignHash(digestAlgorithm.ComputeHash(buffer));

		public byte[] SignData(Stream inputStream) => SignHash(digestAlgorithm.ComputeHash(inputStream));

		public byte[] SignData(byte[] buffer, int offset, int count) => SignHash(digestAlgorithm.ComputeHash(buffer, offset, count));

		public byte[] SignHash(byte[] hash) => Task.Run(() => signHashAsync(hash)).GetAwaiter().GetResult();

		private Task<byte[]> signHashAsync(byte[] hash)
			=> azureKeyVaultService.SignHashAsync(key, hash, digestAlgorithm);

		public bool VerifyData(byte[] dataSigned, byte[] signature) => VerifyHash(digestAlgorithm.ComputeHash(dataSigned), signature);

		public bool VerifyHash(byte[] hashSigned, byte[] signature) {
			var publicKey = azureKeyVaultService.GetPublicKey(key);
			return publicKey.VerifyHash(digestAlgorithm, hashSigned, signature);
		}

		public byte[] Encrypt(byte[] data, EncryptionParameters parameters) {
			var publicKey = azureKeyVaultService.GetPublicKey(key);
			return publicKey.GetEncryptionCsp().Encrypt(data, parameters);
		}

		public byte[] Decrypt(byte[] data, EncryptionParameters parameters) => Task.Run(() => decrypt(data, parameters)).GetAwaiter().GetResult();

		private Task<byte[]> decrypt(byte[] data, EncryptionParameters parameters)
			=> azureKeyVaultService.DecryptAsync(key, data, parameters);
	}
}
