﻿using System;
using System.Threading.Tasks;

namespace Lacuna.Pki.AzureConnector {

	public interface IAzureKeyProvider {

		Task<AzureKey> GetKeyAsync(string keyName);
	}

	public class AzureKeyProvider : IAzureKeyProvider {

		private readonly AzureKeyVaultService azureKeyVaultService;

		public AzureKeyProvider(AzureKeyVaultOptions options, IAzureApiAuthenticator azureApiAuthenticator = null) {
			azureKeyVaultService = new AzureKeyVaultService(() => options, azureApiAuthenticator ?? new BaseAzureApiAuthenticator(() => options));
		}

		public async Task<AzureKey> GetKeyAsync(string keyName)
			=> new AzureKey(azureKeyVaultService, await azureKeyVaultService.GetKeyAsync(keyName));
	}
}
