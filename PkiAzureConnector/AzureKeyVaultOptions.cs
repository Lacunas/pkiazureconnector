﻿namespace Lacuna.Pki.AzureConnector {

	public class AzureKeyVaultOptions {

		public string Endpoint { get; set; }

		public string AppId { get; set; }

		public string AppSecret { get; set; }

		public bool Disabled { get; set; }
	}
}
