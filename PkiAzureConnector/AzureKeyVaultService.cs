﻿using Lacuna.Pki.Asn1;
using Microsoft.Azure.KeyVault;
using Microsoft.Azure.KeyVault.Models;
using Microsoft.Azure.KeyVault.WebKey;
using System;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Lacuna.Pki.AzureConnector {

	internal class AzureKeyVaultService {

		private readonly Func<AzureKeyVaultOptions> optionsAccessor;
		private readonly IAzureApiAuthenticator azureApiAuthenticator;

#pragma warning disable IDE1006 // Naming Styles

		private AzureKeyVaultOptions _options;
		protected AzureKeyVaultOptions Options => _options ??= optionsAccessor.Invoke().CheckEnabled();

		private KeyVaultClient _client;
		protected KeyVaultClient Client => _client ??= new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(azureApiAuthenticator.GetAccessTokenAsync));

#pragma warning restore IDE1006 // Naming Styles

		public AzureKeyVaultService(
			Func<AzureKeyVaultOptions> optionsAccessor,
			IAzureApiAuthenticator azureApiAuthenticator
		) {
			this.optionsAccessor = optionsAccessor;
			this.azureApiAuthenticator = azureApiAuthenticator;
		}

		public Task<KeyBundle> GetKeyAsync(string keyName) => Client.GetKeyAsync(Options.Endpoint, keyName);
		
		public Task<CertificateBundle> GetCertificateAsync(string certificateName) => Client.GetCertificateAsync(Options.Endpoint, certificateName);

		public Task<SecretBundle> GetSecretAsync(string secretName) => Client.GetSecretAsync(Options.Endpoint, secretName);

		public PublicKey GetPublicKey(KeyBundle key) {

			switch (key.Key.Kty) {

				case JsonWebKeyType.Rsa:
				case JsonWebKeyType.RsaHsm:
					return new RSAPublicKey(key.Key.ToRSAParameters());

#if NETSTANDARD2_0 || NET47
				case JsonWebKeyType.EllipticCurve:
				case JsonWebKeyType.EllipticCurveHsm:
					var kvParams = key.Key.ToEcParameters();
					var netParams = new System.Security.Cryptography.ECParameters() {
						Curve = ECCurve.CreateFromValue(getCurveOid(kvParams.Curve)),
						Q = new ECPoint() {
							X = kvParams.X,
							Y = kvParams.Y,
						},
					};
					return new ECPublicKey(netParams);
#endif

				default:
					throw new NotSupportedException($"Unsupported kty: {key.Key.Kty}");
			}
		}

		public async Task<byte[]> SignHashAsync(KeyBundle key, byte[] hash, DigestAlgorithm digestAlgorithm) {
			var result = await Client.SignAsync(key.KeyIdentifier.Identifier, getJsonWebSignatureAlgorithm(key, digestAlgorithm), hash);
			return encodeSignature(result.Result, key);
		}

		public async Task<byte[]> DecryptAsync(KeyBundle key, byte[] data, EncryptionParameters parameters) {
			var result = await Client.DecryptAsync(key.KeyIdentifier.Identifier, getJsonWebEncryptionAlgorithm(key, parameters), data);
			return result.Result;
		}

		private static string getJsonWebSignatureAlgorithm(KeyBundle key, DigestAlgorithm digestAlgorithm) {

			var kty = key.Key.Kty;

			if (key.IsRsa()) {

				if (digestAlgorithm == DigestAlgorithm.SHA256) {
					return JsonWebKeySignatureAlgorithm.RS256;

				} else if (digestAlgorithm == DigestAlgorithm.SHA384) {
					return JsonWebKeySignatureAlgorithm.RS384;

				} else if (digestAlgorithm == DigestAlgorithm.SHA512) {
					return JsonWebKeySignatureAlgorithm.RS512;

				} else {
					throw new NotSupportedException($"Unsupported hash algorithm: {digestAlgorithm}");
				}

			} else if (key.IsEC()) {

				if (digestAlgorithm == DigestAlgorithm.SHA256) {
					return key.Key.CurveName == JsonWebKeyCurveName.P256K ? JsonWebKeySignatureAlgorithm.ES256K : JsonWebKeySignatureAlgorithm.ES256;

				} else if (digestAlgorithm == DigestAlgorithm.SHA384) {
					return JsonWebKeySignatureAlgorithm.ES384;

				} else if (digestAlgorithm == DigestAlgorithm.SHA512) {
					return JsonWebKeySignatureAlgorithm.ES512;

				} else {
					throw new NotSupportedException($"Unsupported hash algorithm: {digestAlgorithm}");
				}

			} else {
				throw new NotSupportedException($"Unsupported key type: {kty}");
			}
		}

		private static string getCurveOid(string curveName) {

			switch (curveName) {

				case JsonWebKeyCurveName.P256:
					return "1.2.840.10045.3.1.7"; // ECCurve.NamedCurves.nistP256 / JsonWebKeyCurveName.P256

				case JsonWebKeyCurveName.P256K:
					return "1.3.132.0.10"; // JsonWebKeyCurveName.P256K

				case JsonWebKeyCurveName.P384:
					return "1.3.132.0.34"; // ECCurve.NamedCurves.nistP384 / JsonWebKeyCurveName.P384

				case JsonWebKeyCurveName.P521:
					return "1.3.132.0.35"; // ECCurve.NamedCurves.nistP321 / JsonWebKeyCurveName.P521

				default:
					throw new NotSupportedException($"Unsupported curve: {curveName}");
			}
		}

		private static byte[] encodeSignature(byte[] signature, KeyBundle key) {

			if (key.IsRsa()) {

				return signature;

			} else if (key.IsEC()) {

				return ECDsaEncoding.EncodeX509Signature(signature);

			} else {
				throw new NotSupportedException($"Unsupported key type: {key.Key.Kty}");
			}
		}

		private static string getJsonWebEncryptionAlgorithm(KeyBundle key, EncryptionParameters parameters) {
			if (!(parameters is RSAEncryption padding)) {
				throw new NotSupportedException($"Unknown encryption parameter: {parameters.GetType()}");
			}
			if (!key.IsRsa()) {
				throw new NotSupportedException($"Not supported key type: {key.Key.Kty}");
			}
			if (padding == RSAEncryption.Pkcs1) {
				return JsonWebKeyEncryptionAlgorithm.RSA15;
			}
			if (padding == RSAEncryption.OaepSHA1) {
				return JsonWebKeyEncryptionAlgorithm.RSAOAEP;
			}
			if (padding == RSAEncryption.OaepSHA256) {
				return JsonWebKeyEncryptionAlgorithm.RSAOAEP256;
			}
			throw new NotSupportedException($"Encryption parameter ({padding}) is not supported");
		}
	}
}
