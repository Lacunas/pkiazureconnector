﻿using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
#if !NETSTANDARD2_0
using Microsoft.Azure;
#endif

namespace Lacuna.Pki.AzureConnector {

	public class AzureTableStorageLogger : ILogger {

		private class LogEntry : TableEntity {

			/*
			 * https://msdn.microsoft.com/library/azure/dd179338.aspx
			 * 
			 * The following characters are not allowed in values for the PartitionKey and RowKey properties:
			 * 
			 * - The forward slash (/) character
			 * - The backslash (\) character
			 * - The number sign (#) character 
			 * - The question mark (?) character
			 * - Control characters from U+0000 to U+001F, including:
			 *   . The horizontal tab (\t) character
			 *   . The linefeed (\n) character
			 *   . The carriage return (\r) character
			 * - Control characters from U+007F to U+009F
			 */
			private static Regex keyRegex = new Regex(@"[/\\#\?\u0000-\u001F\u007F-\u009F]", RegexOptions.Compiled);

			public DateTime UTCDate { get; set; }
			public string Level { get; set; }
			public string Message { get; set; }
			public string Source { get; set; }

			public LogEntry() {
			}

			public static LogEntry Create(LogLevels level, string message, string source) {
				return new LogEntry() {
					PartitionKey = AzureTableStorageLogger.PartitionKey,
					RowKey = Guid.NewGuid().ToString(),
					UTCDate = DateTime.UtcNow,
					Level = level.ToString(),
					Message = message,
					Source = source
				};
			}
		}

		/*
		 * https://msdn.microsoft.com/library/azure/dd179338.aspx
		 * 
		 * Table names must conform to these rules:
		 * - Table names must be unique within an account.
		 * - Table names may contain only alphanumeric characters.
		 * - Table names cannot begin with a numeric character. 
		 * - Table names are case-insensitive.
		 * - Table names must be from 3 to 63 characters long.
		 * - Some table names are reserved, including "tables". Attempting to create a table with a reserved table name returns error code 404 (Bad Request).
		 * 
		 * These rules are also described by the regular expression "^[A-Za-z][A-Za-z0-9]{2,62}$".
		 * Table names preserve the case with which they were created, but are case-insensitive when used.
		 */
		public const string DefaultTableName = "LacunaPkiLog";
		public const string PartitionKey = "pkilog";

#if !NETSTANDARD2_0
		public static AzureTableStorageLogger CreateFromSettingName(string settingName, string tableName = DefaultTableName, LogLevels minLevel = LogLevels.Info) {
			var connectionString = CloudConfigurationManager.GetSetting(settingName);
			return CreateFromConnectionString(connectionString, tableName, minLevel);
		}
#endif

		public static AzureTableStorageLogger CreateFromConnectionString(string connectionString, string tableName = DefaultTableName, LogLevels minLevel = LogLevels.Info) {
			var account = CloudStorageAccount.Parse(connectionString);
			return CreateFromStorageAccount(account, tableName, minLevel);
		}

		public static AzureTableStorageLogger CreateFromStorageAccount(CloudStorageAccount storageAccount, string tableName = DefaultTableName, LogLevels minLevel = LogLevels.Info) {
			var client = storageAccount.CreateCloudTableClient();
			return CreateFromTableClient(client, tableName, minLevel);
		}

		public static AzureTableStorageLogger CreateFromTableClient(CloudTableClient tableClient, string tableName = DefaultTableName, LogLevels minLevel = LogLevels.Info) {
			var table = tableClient.GetTableReference(tableName);
			return CreateFromTableReference(table, minLevel);
		}

		public static AzureTableStorageLogger CreateFromTableReference(CloudTable tableReference, LogLevels minLevel = LogLevels.Info) {
			return new AzureTableStorageLogger(tableReference, minLevel);
		}

#pragma warning disable IDE1006 // Naming Styles
		private readonly CloudTable _table;
		private bool _tableInitialized;
#pragma warning restore IDE1006 // Naming Styles

		private LogLevels minLevel;
		private Task workTask;
		private ConcurrentQueue<Tuple<LogLevels, string, string>> queue;
		private AutoResetEvent enqueueEvent;
		private AutoResetEvent flushEvent;
		private AutoResetEvent flushCompleteEvent;

		private AzureTableStorageLogger(CloudTable table, LogLevels minLevel) {

			this._table = table;
			this.minLevel = minLevel;

			this.queue = new ConcurrentQueue<Tuple<LogLevels, string, string>>();
			this.enqueueEvent = new AutoResetEvent(false);
			this.flushEvent = new AutoResetEvent(false);
			this.flushCompleteEvent = new AutoResetEvent(false);
			this.workTask = Task.Run(() => workAsync());
		}

		public void Configure() {
			PkiConfig.Logger = this;
		}

		public void Log(LogLevels level, string message, string source) {
			if (level < minLevel) {
				return;
			}
			queue.Enqueue(new Tuple<LogLevels, string, string>(level, message, source));
			enqueueEvent.Set();
		}

		private async Task workAsync() {

			Tuple<LogLevels, string, string> entry;
			var flushQueue = new List<TableOperation>();

			while (true) {

				int eventSignalled;
				if (flushQueue.Count == 0) {
					// wait indefinetly
					eventSignalled = WaitHandle.WaitAny(new WaitHandle[] { enqueueEvent, flushEvent });
				} else {
					// wait for 1 second, if nothing happens we'll flush
					eventSignalled = WaitHandle.WaitAny(new WaitHandle[] { enqueueEvent, flushEvent }, TimeSpan.FromSeconds(1));
				}
				var flushRequested = (eventSignalled == 1);
				var timedOut = (eventSignalled == WaitHandle.WaitTimeout);

				// empty queue whatever the case
				while (queue.TryDequeue(out entry)) {
					flushQueue.Add(TableOperation.Insert(LogEntry.Create(entry.Item1, entry.Item2, entry.Item3)));
				}

				// save changes if any of the following holds:
				// - flush requested
				// - timeout occurred (which means there are unflushed entries and we're idle)
				// - unflushed entries >= 10
				if (flushRequested || timedOut || flushQueue.Count >= 10) {

					var table = await GetTableAsync();

					// break the insert into steps because of batch limitations
					// https://msdn.microsoft.com/library/azure/dd894038.aspx
					while (flushQueue.Count > 0) {
						var batch = new TableBatchOperation();
						var batchLen = (flushQueue.Count < 10 ? flushQueue.Count : 10);
						flushQueue.GetRange(0, batchLen).ForEach(o => batch.Add(o));
						try {
							await table.ExecuteBatchAsync(batch);
						} catch {
							// do nothing
						}
						flushQueue.RemoveRange(0, batchLen);
					}
				}

				// signal flush completed if flush requested
				if (flushRequested) {
					flushCompleteEvent.Set();
				}
			}
		}

		public void Flush() {
			flushEvent.Set();
			flushCompleteEvent.WaitOne();
		}

		protected async Task<CloudTable> GetTableAsync() {
			if (!_tableInitialized) {
				await _table.CreateIfNotExistsAsync();
				_tableInitialized = true;
			}
			return _table;
		}
	}
}
