﻿#if NETSTANDARD2_0
using Lacuna.Pki.AzureConnector;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.AzureConnector {

	internal class AzureApiAuthenticatorService : BaseAzureApiAuthenticator {

		public AzureApiAuthenticatorService(IOptions<AzureKeyVaultOptions> optionsAccessor) : base(() => optionsAccessor.Value) {
		}
	}

	internal class AzureProviderService : IAzureKeyProvider, IAzureCertificateProvider {

		private readonly AzureKeyVaultService azureKeyVaultService;

		public AzureProviderService(IOptions<AzureKeyVaultOptions> optionsAccessor, IAzureApiAuthenticator azureApiAuthenticator) {
			this.azureKeyVaultService = new AzureKeyVaultService(() => optionsAccessor.Value, azureApiAuthenticator);
		}

		public async Task<AzureKey> GetKeyAsync(string keyName)
			=> new AzureKey(azureKeyVaultService, await azureKeyVaultService.GetKeyAsync(keyName));

		public Task<PKCertificateWithKey> GetCertificateWithKeyAsync(string certificateName)
			=> azureKeyVaultService.GetCertificateWithKeyAsync(certificateName);
	}
}

namespace Microsoft.Extensions.DependencyInjection {

	public static class AzureConnectorServiceCollectionExtensions {

		public static IAzureKeysBuilder AddAzureKeys(this IServiceCollection services, Action<AzureKeyVaultOptions> setupAction = null) {

			if (setupAction != null) {
				services.Configure(setupAction);
			}

			services.AddSingleton<IAzureApiAuthenticator, AzureApiAuthenticatorService>();
			services.AddTransient<IAzureKeyProvider, AzureProviderService>();
			services.AddTransient<IAzureCertificateProvider, AzureProviderService>();

			return new AzureKeysBuilder(services);
		}
	}

	public interface IAzureKeysBuilder {

		IServiceCollection Services { get; }
	}

	internal class AzureKeysBuilder : IAzureKeysBuilder {

		public IServiceCollection Services { get; }

		public AzureKeysBuilder(IServiceCollection services) {
			this.Services = services;
		}
	}

	public static class AzureKeysBuilderExtensions {

		public static IAzureKeysBuilder Configure(this IAzureKeysBuilder builder, IConfigurationSection configuration) {
			builder.Services.Configure<AzureKeyVaultOptions>(configuration);
			return builder;
		}
	}

}
#endif
