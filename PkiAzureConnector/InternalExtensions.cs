﻿using Microsoft.Azure.KeyVault.Models;
using Microsoft.Azure.KeyVault.WebKey;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.AzureConnector {

	internal static class InternalExtensions {

		public static bool IsRsa(this KeyBundle key)
			=> string.IsNullOrEmpty(key.Key.Kty) || key.Key.Kty == JsonWebKeyType.Rsa || key.Key.Kty == JsonWebKeyType.RsaHsm;

		public static bool IsEC(this KeyBundle key)
			=> key.Key.Kty == JsonWebKeyType.EllipticCurve || key.Key.Kty == JsonWebKeyType.EllipticCurveHsm;

		public static bool IsConfigured(this AzureKeyVaultOptions options)
			=> !string.IsNullOrEmpty(options.Endpoint)
			|| !string.IsNullOrEmpty(options.AppId)
			|| !string.IsNullOrEmpty(options.AppSecret);

		public static AzureKeyVaultOptions CheckEnabled(this AzureKeyVaultOptions options) {
			if (options.Disabled) {
				throw new InvalidOperationException("Azure key vault integration is disabled");
			}
			if (!options.IsConfigured()) {
				throw new InvalidOperationException("Azure key vault integration is not configured");
			}
			return options;
		}

		public static async Task<PKCertificateWithKey> GetCertificateWithKeyAsync(this AzureKeyVaultService azureKeyVaultService, string certificateName) {
			var certBundle = await azureKeyVaultService.GetCertificateAsync(certificateName);
			var cert = PKCertificate.Decode(certBundle.Cer);
			var key = new AzureKey(azureKeyVaultService, await azureKeyVaultService.GetKeyAsync(certBundle.KeyIdentifier.Name));
			return key.GetCertificateWithKey(cert);
		}
	}
}
