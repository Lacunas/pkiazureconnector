﻿--------------------------------------------------------------------------------
                           Lacuna PKI Azure Connector
--------------------------------------------------------------------------------

Overview
--------

This package enables the following integrations between the Lacuna PKI SDK and
the Microsoft Azure storage service:

* Compress and decompress CAdES signatures storing the CRLs and certificates in
  a Blob Storage
* Perform and validate CAdES signatures with revocation references but without
  revocation values (CAdES-X Type 1 or ICP-Brasil AD-RV) by storing the
  correspondent values on a Blob Storage
* Send log messages generated by the SDK to a Table Storage

NOTE: In order to maximise compatibility, this package depends on an old version
of the Azure Storage package version, but we STRONGLY RECOMMEND that you install
the latest version (currently the latest version with confirmed compatibility is
4.3.0, but any future 4.x versions should also work).

For more information regarding the Lacuna PKI SDK, please visit:

	http://pki.lacunasoftware.com/

Azure Storage credentials
-------------------------

The first thing you'll need is to create an Azure storage account (obviously)
and get access credentials, called "Access Keys" in Azure-vernacular. If haven't
already done that, the following link can guide you through the process:

	https://azure.microsoft.com/en-us/documentation/articles/storage-create-storage-account/

By the end of that process you should have a connection string similar to:

	DefaultEndpointsProtocol=https;AccountName=youraccount;AccountKey=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX==

The best place to put that string is in an appSetting in your web.config:

	<appSettings>
		<add key="AzureStorage" value="DefaultEndpointsProtocol=https;AccountName=youraccount;AccountKey=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX==" />
	</appSettings>

But you may also keep it in your code, as we'll see in a bit.

AzureBlobStorageStore
---------------------

The AzureBlobStorageStore implements the ISimpleStore interface, which is used
by the SDK whenever a storage is needed to store and/or retrieve objects, for
instance when compressing CAdES signatures.

If you stored your Azure Storage connection string in a appSettings entry as
suggested above, all you need to do to instantiate a AzureBlobStorageStore is:

	var store = AzureBlobStorageStore.CreateFromSettingName("AzureStorage"); // or whatever else you put in the "key" attribute of the appSetting

If you'd rather have the connection string in your code, you can:

	var store = AzureBlobStorageStore.CreateFromConnectionString("DefaultEndpointsProtocol=https;AccountName=youraccount;AccountKey=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX==");

By default, the objects are stored in a storage named "lacuna-pki-store", but
you can change that:

	var store = AzureBlobStorageStore.CreateFromSettingName("AzureStorage", "my-container");

Once you have an instance of AzureBlobStorageStore associated with your storage
account and container, you can, for instance, compress and decompress a CAdES
signature:

	byte[] precomputedSignature = ...; // any CAdES signature, not necessarily generated with the SDK
	var compressedSignature = CadesSignatureCompression.Compress(precomputedSignature, store);
	var decompressedSignature = CadesSignatureCompression.Decompress(compressedSignature, store);
	// precomputedSignature and decompressedSignature will be the same

AzureTableStorageLogger
-----------------------

The AzureTableStorageLogger class is used to send log messages generated by the
SDK to an Azure Table. You instantiate the class in the same manner as the
AzureBlobStorageStore:

	var logger = AzureTableStorageLogger.CreateFromSettingName("AzureStorage"); // or whatever else you put in the "key" attribute of the appSetting

By default, logs are stored in a table named "LacunaPkiLog", but you can
override that setting:

	var logger = AzureTableStorageLogger.CreateFromSettingName("AzureStorage", "MyTable");

You can also specify the minimum level to log (the default is "Info"):

	var logger = AzureTableStorageLogger.CreateFromSettingName("AzureStorage", minLevel: LogLevels.Trace); // this would log A LOT, use only for diagnostics

Once you have an instance of AzureTableStorageLogger, simply call the Configure
method and you're good to go:

	logger.Configure();

In short, you generally do:

	AzureTableStorageLogger.CreateFromSettingName("AzureStorage").Configure();

Source code
-----------

This package is open source. Feel free to fork it if you need to make any
customizations:

	https://bitbucket.org/Lacunas/pkiazureconnector
