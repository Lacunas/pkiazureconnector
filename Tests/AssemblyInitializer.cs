﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lacuna.Pki.AzureConnector.Tests {

	[TestClass]
	public static class AssemblyInitializer {

		[AssemblyInitialize]
		public static void InitializeAssembly(TestContext context) {
			PkiConfig.LoadLicense(Convert.FromBase64String(Config.PkiSdkLicense));
		}
	}
}
