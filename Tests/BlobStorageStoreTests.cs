﻿using Lacuna.Pki.Cades;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.AzureConnector.Tests {

	[TestClass]
	public class BlobStorageStoreTests {

		CloudBlobContainer container;

		[TestInitialize]
		public async Task InitializeAsync() {
			var account = CloudStorageAccount.Parse(Config.ConnectionString);
			var client = account.CreateCloudBlobClient();
			container = client.GetContainerReference(String.Format("lacuna-pki-azure-connector-tests-{0}", DateTime.Now.Ticks));
			await container.CreateAsync();
		}

		[TestCleanup]
		public async Task CleanupAsync() {
			if (container != null) {
				await container.DeleteIfExistsAsync();
			}
		}

		[TestMethod]
		public void CompressAndDecompress() {

			for (int i = 1; i <= 2; i++) {
				var store = AzureBlobStorageStore.CreateFromConnectionString(Config.ConnectionString, container.Name);
				byte[] precomputedSignature = File.ReadAllBytes(String.Format("Signature{0}.p7s", i));
				var compressedSignature = CadesSignatureCompression.Compress(precomputedSignature, store);
				var decompressedSignature = CadesSignatureCompression.Decompress(compressedSignature, store);
				CollectionAssert.AreEqual(precomputedSignature, decompressedSignature);
			}

			// Both signatures contain the same 3 certificates and 2 CRLs, so the bucket folder should
			// have only 5 objects, any more than that means unecessary redundancy

			//var listObjectsRequest = new ListObjectsRequest() {
			//	BucketName = bucketName,
			//	Prefix = basePath + "/"
			//};
			//var response = s3Client.ListObjects(listObjectsRequest);
			//Assert.AreEqual(5, response.S3Objects.Count);
		}
	}
}
