using System;
using System.Collections.Generic;
using System.Text;

namespace Lacuna.Pki.AzureConnector.Tests {

	public static class Config {

		// Licensee: Lacuna Lab2, Expiration: 2020-12-05 03:00:00Z, VersionNumberLimit: (no limit), VersionReleaseDateLimit: (no limit)
		public const string PkiSdkLicense = "AxAArrO7WBtvhkK6gp6kdATsQwsATGFjdW5hIExhYjIIAIDUUnGLdtcICAAAeNLayZjYCAAAAAAAAAQAfwAAAAABKJwczmb4QfHlV5+m5z46uL+zH1N5N4eRWe3IN/D6iNfcFnpOn1d42LCpNpf2F0iX3PfYFeH+mWo747VGnwMfUITANvEjpifOSInQ2VHcrgU6whNizl80ugsAlk650idBhI5vHsy9ySMaEJyk6ItW4lcVrpuiNLcsUOBzOi+OWbJmUMt9CRMgYZQi9TQ47K+LCOrvL9HBZIRxRV76k6IMYxGjECJ/L9RbE1EwOq1Nft2C4ffyt38hOiD0wYc2KbhOaNvyOVDzHSSkbDIlTTN8o2tNewGK6M6vO7QSzSharN1WMaL1VIcl/SOFmg2YCZuZqONqLfz79cJDBIsqnJSt+Q==";

		public const string ConnectionString = "DefaultEndpointsProtocol=https;AccountName=lacunadev;AccountKey=I1vSijjx3LyiQbvpdXloPSKrGnwknlmxQfFVjInoI/2D8YzP2Wy0itxJDvccBGG7ApVSa+4E9ki3g78UJH/B/w==;EndpointSuffix=core.windows.net";

		public const string KeyVaultEndpoint = "https://lacuna-demo.vault.azure.net/";

		public const string AppId = "626c42b0-7145-436c-97e5-368be2e64d9d";

		public static string AppSecret => throw new Exception("Fill the AppSecret config!");
	}
}
