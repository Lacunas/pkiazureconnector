﻿using Lacuna.Pki.Pades;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.AzureConnector.Tests {

	[TestClass]
	public class KeyVaultTests {

		[TestMethod]
		public async Task SignPdfWithAzureKey() {
			var azureKeyProvider = new AzureKeyProvider(getOptions());
			var azureKey = await azureKeyProvider.GetKeyAsync(Util.ServerCertificateKeyName);
			var certWithKey = azureKey.GetCertificateWithKey(Util.ServerCertificate);
			signPdf(certWithKey);
		}

		[TestMethod]
		public async Task SignPdfWithAzureCertificate() {
			var azureCertProvider = new AzureCertificateProvider(getOptions());
			var certWithKey = await azureCertProvider.GetCertificateWithKeyAsync(Util.ServerCertificateName);
			signPdf(certWithKey);
		}

		private void signPdf(PKCertificateWithKey certWithKey) {

			var signer = new PadesSigner();
			signer.SetPdfToSign("sample.pdf");
			signer.SetSigningCertificate(certWithKey);
			signer.SetPolicy(PadesPoliciesForGeneration.GetPadesBasic(Util.LacunaTestTrustArbitrator));
			signer.ComputeSignature();
			var signedPdf = signer.GetPadesSignature();

			var signature = PadesSignature.Open(signedPdf);
			Assert.AreEqual(1, signature.Signers.Count);

			signature.ValidateSignature(signature.Signers.First(), PadesPoliciesForValidation.GetPadesBasic(Util.LacunaTestTrustArbitrator));
		}

		[TestMethod]
		public void InstancingShouldNotThrowIfDisabled() {
			try {
				new AzureKeyProvider(getOptions(options => { options.Disabled = true; }));
			} catch {
				Assert.Fail("Instancing AzureKeyProvider with disabled options should not throw");
			}
		}

		[TestMethod]
		public void InstancingShouldNotThrowIfNotConfigured() {
			try {
				new AzureKeyProvider(new AzureKeyVaultOptions());
			} catch {
				Assert.Fail("Instancing AzureKeyProvider with unconfigured options should not throw");
			}
		}

		[TestMethod]
		public async Task UsingShouldThrowIfDisabled() {
			Exception exception = null;
			try {
				var provider = new AzureKeyProvider(getOptions(options => { options.Disabled = true; }));
				await provider.GetKeyAsync(Util.ServerCertificateKeyName);
			} catch (Exception ex) {
				exception = ex;
			}
			Assert.IsNotNull(exception, "Using AzureKeyProvider with disabled options did not throw");
			Assert.IsInstanceOfType(exception, typeof(InvalidOperationException));
		}

		[TestMethod]
		public async Task UsingShouldThrowIfNotConfigured() {
			Exception exception = null;
			try {
				var provider = new AzureKeyProvider(new AzureKeyVaultOptions());
				await provider.GetKeyAsync(Util.ServerCertificateKeyName);
			} catch (Exception ex) {
				exception = ex;
			}
			Assert.IsNotNull(exception, "Using AzureKeyProvider with unconfigured options did not throw");
			Assert.IsInstanceOfType(exception, typeof(InvalidOperationException));
		}

		[TestMethod]
		public async Task AccessTokensShouldBeReused() {
#if DEBUG
			var options = getOptions();

			// Sharing a single AzureApiAuthenticator between AzureKeyProviders is a prerequisite for reusing tokens
			var azureApiAuthenticator = new AzureApiAuthenticator(options);

			for (int i = 0; i < 2; i++) {
				var azureKeyProvider = new AzureKeyProvider(options, azureApiAuthenticator);
				var key = await azureKeyProvider.GetKeyAsync(Util.ServerCertificateKeyName);
				var digestAlg = Util.DefaultDigestAlgorithm;
				key.GetSignatureCsp(digestAlg).SignHash(Util.GetRandomHash(digestAlg));
			}

			Assert.AreEqual(1, azureApiAuthenticator.TokenRequestCount);
#else
			Assert.Inconclusive("This test can only be runned in Debug");
#endif
		}

		private AzureKeyVaultOptions getOptions(Action<AzureKeyVaultOptions> configureOptions = null) {
			var options = new AzureKeyVaultOptions() {
				Endpoint = Config.KeyVaultEndpoint,
				AppId = Config.AppId,
				AppSecret = Config.AppSecret,
			};
			if (configureOptions != null) {
				configureOptions.Invoke(options);
			}
			return options;
		}
	}
}
