﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System.Linq;
using System.Threading.Tasks;

namespace Lacuna.Pki.AzureConnector.Tests {

	[TestClass]
	public class TableStorageLoggerTests {

		private class LogEntry : TableEntity {
			public DateTime UTCDate { get; set; }
			public string Level { get; set; }
			public string Message { get; set; }
		}

		private CloudTable logTable;

		[TestInitialize]
		public async Task InitializeAsync() {
			var account = CloudStorageAccount.Parse(Config.ConnectionString);
			var client = account.CreateCloudTableClient();
			logTable = client.GetTableReference("LogTest" + DateTime.Now.Ticks);
			await logTable.CreateAsync();
			AzureTableStorageLogger.CreateFromConnectionString(Config.ConnectionString, logTable.Name).Configure();
		}

		[TestCleanup]
		public async Task CleanupAsync() {
			if (logTable != null) {
				await logTable.DeleteIfExistsAsync();
			}
		}

		[TestMethod]
		public async Task SingleThreadTestAsync() {

			var iterations = 1000;

			var sw = Stopwatch.StartNew();
			for (int i = 0; i < iterations; i++) {
				PkiConfig.Logger.Log(LogLevels.Info, "Message #" + i, "Lacuna.Pki.Class" + i);
			}
			sw.Stop();
			PkiUtil.FlushLog();

			// Execution of the loop should be nearly instantaneous (excluding the call to Finalize, which flushes the log) 
			// Therefore, we specify a minimum rate of 1 log / milisecond.
			Assert.IsTrue(sw.Elapsed < TimeSpan.FromMilliseconds(iterations));

			var indexes = new List<int>();
			var regex = new Regex(@"Message #(\d+)");

			var query = new TableQuery<LogEntry>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, AzureTableStorageLogger.PartitionKey));

			var results = await logTable.ExecuteQuerySegmentedAsync(query, new TableContinuationToken());

			foreach (var row in results) {
				var match = regex.Match(row.Message);
				if (match.Success) {
					indexes.Add(int.Parse(match.Groups[1].Value));
				}
			}

			Assert.AreEqual(iterations, indexes.Count);
			Enumerable.Range(0, iterations).ToList().ForEach(i => Assert.IsTrue(indexes.Contains(i)));
		}

		[TestMethod]
		public async Task MultiThreadTestAsync() {

			var iterations = 1000;

			var sw = Stopwatch.StartNew();
			Parallel.For(0, iterations, i => PkiConfig.Logger.Log(LogLevels.Info, "Message #" + i, "Lacuna.Pki.Class" + i));
			sw.Stop();
			PkiUtil.FlushLog();

			// Execution of the loop should be nearly instantaneous (excluding the call to Finalize, which flushes the log) 
			// Therefore, we specify a minimum rate of 1 log / milisecond.
			Assert.IsTrue(sw.Elapsed < TimeSpan.FromMilliseconds(iterations));

			var indexes = new List<int>();
			var regex = new Regex(@"Message #(\d+)");

			var query = new TableQuery<LogEntry>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, AzureTableStorageLogger.PartitionKey));
			var results = await logTable.ExecuteQuerySegmentedAsync(query, new TableContinuationToken());

			foreach (var row in results) {
				var match = regex.Match(row.Message);
				if (match.Success) {
					indexes.Add(int.Parse(match.Groups[1].Value));
				}
			}

			Assert.AreEqual(iterations, indexes.Count);
			Enumerable.Range(0, iterations).ToList().ForEach(i => Assert.IsTrue(indexes.Contains(i)));
		}
	}
}
